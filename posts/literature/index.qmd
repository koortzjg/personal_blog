---
title: "Using `Zotero` and `Obsidian` for Reference Management"
author: "Hardi Koortzen"
date: "2023-06-30"
categories: [Zotero, Obsidian, reference management]
---

Over the years, I have tried and failed to keep a record of literature articles.
I have tried numerous ways. This ranged from mind maps to Word documents and
several reference managers. I have attempted to use reference managers such as
[`Jabref`](https://www.jabref.org/), [`Mendeley`](https://www.mendeley.com/),
and [`Zotero`](https://www.zotero.org/). Since this is not a comparison post,
each has advantages and disadvantages. The latter two offered online storage for
PDF files. I like that, although the free versions have limited storage space.
Around 2016 I finally settled on just one reference manager, `Zotero`. I also
finally decided to support `Zotero` by buying unlimited storage. Since `Zotero`
is open source, using it as a reference manager and paying for storage; I do a
bit to keep it going. I now have a *safety* net since all my files are stored
online, reducing the risk of losing these files.

Now that I have all the files in one place, I still wasn't making notes when
reading the files. `Zotero` can save highlights and notes with the documents.
Sometimes, one requires to do more with these notes. Earlier this year, 2023, I
came across [`Obsidian`](https://obsidian.md/). Again an open-source project.
`Obsidian` allows one to create `markdown` files type notes. One can link to
other references and add keywords. `Obsidian` can also visualise the
relationships between notes. I am not doing it justice here, so please head to
the website to learn more about this. It is a good way of visualising the notes
in one's references and the links between them. See this
[post](https://obrl-soil.github.io/posts/2023-03-28_obsidian-setup-2023/) on how
to set up `Obsidian`.

Since I am not using the sync function and not moving the stored files to a
different location, I needed another way to ensure that I can access my
literature across various locations. Probably not as intended, but `Git` to the
rescue, see this [post](../version_control_git/index.qmd) on version control and
`Git`. I am unsure whether I first initiated the `Git` repository or the
`Obsidian` project; however, one must do both. Unfortunately, I cannot
completely follow the guide on setting up `Obsidian`
[here](https://obrl-soil.github.io/posts/2023-03-28_obsidian-setup-2023/).
Moving all my files is impossible without breaking the syncing with `Zotero`
online. Therefore my setup does not allow me to open the reference's associated
`.pdf` file. This is not an issue since I can always get this from my online
library at `Zotero`. Using the power of `Git,` I can also keep all the settings
and extensions up to date across my devices.

I use the following extensions in `Zotero`:

1.  [`Better BibTex`](https://retorque.re/zotero-better-bibtex/) assigning
    unique keys and automating the export of one's library to a `.bib` file.
2.  [`Zotfile`](http://zotfile.com/) offers a one-click solution for moving a
    file into a designated folder and renaming it according to a given pattern.

I use the following extensions in `Obsidian`:

1.  [`Admonition`](https://github.com/javalent/admonitions) is a tool that
    allows one to create attention-grabbing callouts, tips, warnings and other
    informative blocks within one's notes.
2.  [`Citations`](https://github.com/hans/obsidian-citation-plugin) picks up the
    `.bib` file that `Better BibTex` created and allows one to generate a
    markdown-format literature note for each reference with a matching name,
    some excellent metadata, and a pre-formatted space all ready for one to add
    notes.
3.  [`Obsidian Graphviz`](https://github.com/QAMichaelPeng/obsidian-graphviz)
    uses the local `Graphviz` `dot` executable for rendering.
4.  [`Projects`](https://github.com/marcusolsson/obsidian-projects) create
    projects from folders and Dataview queries.
5.  [`Zotero Integration`](https://github.com/mgmeyers/obsidian-zotero-integration)
    insert and import citations, bibliographies, notes, and PDF annotations from
    `Zotero` into `Obsidian`.

With this setup, I do not have an excuse to read and keep notes on the
references in my library. Maybe time to start since, as of today, there are 969
items in my library. Not all of these require to have notes. However, that is
still a lot of notes that I need to create. I will complete this in about 2.5
years if I can read one daily. Wow, I better get started, then.