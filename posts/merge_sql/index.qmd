---
title: "Using `MERGE` in `SQL Server`"
author: "Hardi Koortzen"
date: "2023-07-21"
categories: [SQL, SQL Server]
---

Recently I found the `MERGE` `SQL Server` statement. This statement can replace
the `UPDATE`, `DELETE`, and `INSERT` statements when inserting and updating a
table with new values. This sequence of statements is used in many of the
scripts I have created for work. That was how the previous analyst wrote
scripts, and I just adopted that since I wasn't trained in `SQL`.

First, look at the original `UPDATE`, `DELETE`, and `INSERT` statements. We want
to update and add new users that have been created or updated. We can have a
table `Users` containing information about the users. We have information about
when they last logged in, `LatestLogin`, when they were created, `Created`,
their unique id, `LocalId`, etc. From our source data, we obtain all the users;
let's call that table `#users`. The `#users` table has the following fields:

1.  `user_name`,
2.  `email`,
3.  `created_at`,
4.  `latest_login_at`, and
5.  `local_id`

To update the existing users, we need the `UPDATE` statement to update the users
already in the table.

``` sql
UPDATE u
SET u.LatestLogin = k.latest_login_at
FROM Users AS u
INNER JOIN #users AS k
    ON u.LocalId = k.local_id
```

Next, we need to remove these users from the `#users` table before inserting the
new users.

``` sql
DELETE k
FROM #users AS k
INNER JOIN Users AS u
    ON k.local_id = u.LocalId
```

Finally, we insert the *new* users.

``` sql
INSERT INTO Users (
    [Name],
    Email,
    Created,
    LatestLogin,
    LocalId
)
SELECT [user_name],
    email,
    created_at,
    latest_login_at,
    local_id
FROM #users
```

This sequence is straightforward and can easily be understood by others. Why
would you want to change this? The `MERGE` statement provides other valuable
operations and is also concise. Now for the `MERGE` statement.

``` sql
MERGE Users AS t
USING #users AS s
    ON t.LocalId = s.local_id
WHEN MATCHED
    THEN
        UPDATE
        SET t.LatestLogin = s.latest_login_at
WHEN NOT MATCHED BY TARGET
    THEN
        INSERT (
            [Name],
            Email,
            Created,
            LatestLogin,
            LocalId
            )
        VALUES (
            [user_name],
            email,
            created_at,
            latest_login_at,
            local_id
            );
```

The statement process is as follows:

1.  The rows from the source table, `#users`, with a match in the target table,
    `Users`, the `MERGE` statement updates the values in the `Users.LatestLogin`
    column with the values in the `#users.latest_login_at` column.
2.  The `MERGE` statement then inserts the rows not in the `Users` table.

Another helpful feature of the `MERGE` statement is that it can remove unwanted
rows from the table. For example, there is a table containing all the
subscriptions to your products called `Contracts`. We are only interested in
active/live subscriptions for reporting purposes. That is subscriptions that
have not been cancelled or have expired. Here we need to do three things:

1.  Update the subscription phase, the start and end dates, and when the report
    was created.
2.  Insert new subscriptions.
3.  Remove cancelled or expired subscriptions.

Instead of writing several `UPDATE`, `INSERT`, and `DELETE` statements, we can
use the `MERGE` statement.

``` sql
MERGE Subscriptions AS t
USING (
    SELECT account_id,
        isbn,
        contract_number,
        trial_start_date,
        contract_start_date,
        end_date AS contract_end_date,
        CASE 
            WHEN contract_start_date > GETDATE()
                THEN 'Trial'
            WHEN contract_start_date >= DATEADD(MONTH, - 1, GETDATE())
                THEN 'New'
            WHEN contract_start_date >= DATEADD(MONTH, - 3, GETDATE())
                THEN 'Early'
            WHEN [start_date] <= GETDATE()
                AND DATEDIFF(DAY, GETDATE(), end_date) <= 90
                THEN 'Approaching Renewal'
            WHEN [start_date] <= GETDATE()
                AND DATEDIFF(DAY, GETDATE(), end_date) <= 90
                AND contract_status IN ('NREN')
                THEN 'Ending'
            WHEN [start_date] <= GETDATE()
                AND DATEDIFF(DAY, GETDATE(), end_date) > 90
                THEN 'Main'
            END AS contract_phase,
        CAST(GETDATE() AS DATE) AS report_run
    FROM Contracts
    WHERE EOMONTH([start_date]) = EOMONTH(GETDATE(), - 1)
        AND end_date > EOMONTH(GETDATE(), - 1)
        AND is_cancelled = 0
        AND original_contract_number IS NULL
    ) AS s
    ON t.account_id = s.account_id
        AND t.isbn = s.isbn
WHEN MATCHED
    THEN
        UPDATE
        SET t.contract_number = s.contract_number,
            t.trial_start_date = s.trial_start_date,
            t.contract_start_date = s.contract_start_date,
            t.contract_end_date = s.contract_end_date,
            t.contract_phase = s.contract_phase,
            t.report_run = s.report_run
WHEN NOT MATCHED BY TARGET
    THEN
        INSERT (
            account_id,
            isbn,
            contract_number,
            trial_start_date,
            contract_start_date,
            contract_end_date,
            contract_phase,
            report_run
            )
        VALUES (
            account_id,
            isbn,
            contract_number,
            trial_start_date,
            contract_start_date,
            contract_end_date,
            contract_phase,
            report_run
            )
WHEN NOT MATCHED BY SOURCE
    THEN
        DELETE;
```

As seen above, the `MERGE` statement is concise and easy to understand. We could
update, insert, and delete rows from the table in just a few lines. Using the
`MERGE` statement also allows us to easily add more columns as the requirements
arise. Reducing the number of places someone needs to edit also reduces the risk
of errors.

Another use case will be to update a column value when not in the *source* data.
Unlike the `DELETE` above, we can use `SET` here.

``` sql
MERGE Subscriptions AS t
USING (
    SELECT account_id,
        isbn
    FROM Subscriptions
    WHERE number_of_teachers_access_subscription > 0
        AND number_of_students_access_subscription > 19
    ) AS s
    ON t.account_id = s.account_id
        AND t.isbn = s.isbn
WHEN MATCHED
    THEN
        UPDATE
        SET t.enrolled = 'Y'
WHEN NOT MATCHED BY SOURCE
    THEN
        UPDATE
        SET t.enrolled = 'N';
```

Above, we can set a conditional statement to *true* or *Y* when there is a
match, and for the rest, we can `SET` it to *false* or *N*. Again, this is done
concisely without the need to create temporary tables. We can ensure that all
the statuses are correct since there can be a change in the conditional
statement for the account and product combination.

`MERGE` is a versatile statement with many use cases. It is concise and clearly
demonstrates the intent of the action taken. This is a `SQL Server` statement
unavailable in the other `SQL` versions. The `MERGE` statement needs to be
terminated with a semi-colon (;).

I have found this very useful. It allows me to not use temporary tables, as the
update and insert are done in one step. The tricky part is to find the correct
`ON` clause. I say tricky because if the `ON` clause differed in the case above,
we might have deleted the needed rows. Only time will tell whether I will revert
to using the `MERGE` statement. I am sure there will be situations where the
`UPDATE`, `DELETE`, and `INSERT` statements work better.